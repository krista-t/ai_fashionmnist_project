% \documentclass[a4paper]{article}
\documentclass[a4paper, twocolumn]{article}


\usepackage[english]{babel}

% Set page size and margins
% Replace `letterpaper' with `a4paper' for UK/EU standard size
\usepackage[a4paper,top=2cm,bottom=2cm,left=2cm,right=2cm,marginparwidth=1.75cm]{geometry}

% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{makecell}
\usepackage[backend=biber]{biblatex}
\addbibresource{./refs.bib}


\title{%
Image Classification Using Convolutional Neural Network and the effect of Autoencoder on the prediction of images in the Fashion-MNIST Dataset \\
  \large}
\author{%
Astthor Arnar Bragason, Alexandra Meszaros, \\
Kristina Tomicic, Marwane Ghalila}

\begin{document}
\maketitle

\begin{abstract}
Autoencoders have drawn lots of attention in the field of image processing \cite{denoise}. As the target output of an Autoencoder is the same as its input, Autoencoders can for example be used for data compression and data denoising \cite{progimg}.
In this paper, we focus on trying to find out how the prediction rate of a Convolutional Neural Network is affected after running the data through an Autoencoder. After evaluating the images’ before- and after state, we confirm that the Autoencoder influences the predictions negatively. 
\end{abstract}



% kernel is filter 


\section{Introduction}

Machine learning models  such as Convolutional Neural networks (CNNs) \cite{deeplearning} are based on deep learning architecture and are specifically used for image recognition tasks \cite{introtocnn}. 
\\
\\
\noindent In our research, we use CNN to classify images of clothes using the Fashion MNIST dataset \cite{dataset}. Images can consist of large amounts of data, can therefore be slow to transfer and it can be expensive to store large amounts of image data. In order to facilitate the transfer process and reduce storage costs, we are interested to see how data that has been reconstructed  by an autoencoder might affect the CNN’s ability to recognize images. In machine learning, an autoencoder is an unsupervised learning algorithm that encodes an image, reducing its dimensions, and then tries to decode it to the image's original state \cite{progimg}. 



\subsection{Research question}
The focus of this paper is the following research question: How accurately can we predict different clothing examples using a Convolutional Neural Network before and after the data has been processed through an autoencoder? Furthermore, since images may become noisy during transfer, we decided to utilise the autoencoder’s other feature, namely denoising, which leads to our research’s subquestion: Can we improve the prediction rate of noisy images after using an autoencoder for denoising? 


\noindent \textbf{Hypothesis:} Processing Fashion MNIST dataset through Autoencoder negatively affects the prediction rate.

\noindent \textbf{Null-hypothesis:} Processing the Fashion MNIST dataset through autoencoder does not impact the prediction rate negatively.

\noindent \textbf{Hypothesis 2:} Processing noisy images first through autoencoder and then through a CNN, outperforms noisy image classification using just a CNN.


\noindent \textbf{Null-hypothesis 2:} Processing noisy images first through autoencoder and then through a CNN, does not outperform noisy image classification using just a CNN.




\section{Methods}

We use Nunamaker’s Multi-methodological framework, which is a systematic way to approach complex research problems \cite{nun}. It emphasises the importance of using multiple methods and perspectives to gather and analyse data, and using that information for decision making.  Our research process follows Nunamaker’s approach, as during the process we go back and forth between the steps of the research; for example after evaluating our first results, we go back to rephrase our research question and hypothesis.
\\
\\
\noindent For statistical analysis, we used a method to check the variance between the count of each class of clothing in the training sets. Then for data preparation we split the training dataset into training and validation datasets, using stratification. Stratified sampling is a method often used for partitioning population data into subgroups, as it reduces sampling errors\cite{baeldung}.
\\
\\
\noindent To classify images we use a Convolutional Neural Network (CNN) which is particularly well suited for image-related tasks, because they are able to automatically learn spatial hierarchies of features from the data \cite{class}.  We also explore the topic about autoencoders’ two widely used use cases, namely compressing data \cite{ae}, which helps to reduce the storage usage, and removing noise from the original data. Autoencoders consist of an encoder and a decoder. Encoding an image reduces its dimension, and a decoder restores the number of dimensions based on the encoded data. Another application of autoencoder is image denoising. In this task, we treat the autoencoder as a nonlinear function that can eliminate the effect of noises in the image. 

\section{Analysis}
\subsection{Data}
We use the Fashion MNIST dataset, which includes a total 70,000 images, and consists of  a training set with 60,000 images and a test set with 10,000 images. In order to estimate the performance of a model during training, we divide the training set, using stratified sampling, into training and validation datasets, with 50,000 images and 10,000 images respectively.  Each image is a 28x28 grayscale image, which is associated with a label from 10 classes \cite{dataset}.
\\
\\
\noindent \textbf{Descriptive statistics}
Each image in both the training and test set is assigned to one of the following ten labels:
['T-shirt/Top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle Boot']
\\
\\
\noindent Counting how many examples there are of each label class in the training, validation and test datasets, we check for variance between the count of each label class in the datasets. This is to make sure that splitting the training dataset using stratification works as expected, meaning that in each dataset we  have the same amount of images in each label class. For example, after splitting the training dataset, we have 5,000 images of each class in the training dataset.

\subsection{Structure of models}
\textbf{CNN}
\noindent Our Convolutional Neural Network’s architecture is inspired by a previously built model, made by the data scientist Eamon Fleming \cite{fleming}. Using his model as inspiration we experiment by changing the structure to improve our results as explained in the ‘Process’ section, and came up with the final structure that can be explained in two parts:
\begin{itemize}
  \item Feature extraction, which includes four 2D convolutional layers, a single BatchNormalization layer after the first convolutional layer, followed by a MaxPooling2D layer, and finally a Flatten layer.
  \item The network, which includes three dense layers.
\end{itemize}

\noindent In both parts of the CNN, there is a dropout layer separating each layer, refer to Table 1 to see dropout rate.

\begin{table}[h!]
\tiny
\centering
 \begin{tabular}{|l|l|l|l|l|} 
 \hline
 Layer & Type & Nodes & Filter & Activation \\ [0.5ex] 
 \hline\hline
 1 & Conv2D & 32 & 3x3 & relu \\ 
 1 & BatchNormalization & - & - & - \\
 1 & Dropout & 0.2 & - & - \\
2 & Conv2D & 32 & 3x3 & relu \\ 
 2 & Dropout & 0.2 & - & - \\
  3 & Conv2D & 24 & 3x3 & relu \\ 
 3 & Dropout & 0.3 & - & - \\
  4 & Conv2D & 64 & 3x3 & relu \\ 
  4 & MaxPool2D & - & 2x2 & - \\ \hline
 4 & Dropout & 0.2 & - & - \\
  4 & Flatten & - & - & - \\
    4 & Dense & 128 & - & relu \\
       4 & Dropout & 0.3 & - & - \\
  5 & Dense & 64 & - & relu \\
   6 & Dropout & 0.05 & - & - \\
  6 & Dense & 10 & - & softmax \\
 \hline
 \end{tabular}
 
 \caption{Structure of Convolutional Neural Network}
\label{table:1}
\end{table}

\noindent As we can see it in Table 1, all layers use the Rectified Linear Unit (ReLU) activation function, except the last 10 neuron output layers representing the classification output, which uses the softmax activation function. Compiling the model, we use the optimizer Adam, and the Categorical Crossentropy loss function. Fitting the model, we use the training and validation datasets, 30 epochs, 128 batch size and have a list of callbacks. Callbacks consist of a checkpoint, saving the best end-of-epoch model based on the validation accuracy, and a scheduler function that updates the learning rate.
\\
\\
\noindent \textbf{Autoencoder}
\noindent We specify the model’s hyper-parameters of each convolution layer that we implement (kernel size, stride, padding), refer to Table 2. It consists of two parts. The first part (encoding) is made of convolution and pooling layers  and the second part (decoding) is a mirror version of the encoding, except that instead of pooling layers it has upsampling layers \cite{keras}. 


\begin{table}[h!]
\tiny
\centering
\begin{tabular}{|l|l|l|l|l|l|}
\hline
 & Layer & Type & Nodes & Filter & Activation \\  [0.5ex] 
 \hline\hline
 
\multirow{Encoder} & 1 & Conv2D & 64 & 3x3 & relu \\ 
  & 1 & Conv2D & 32 & 3x3 & relu \\
 &  1 & MaxPool2D & - & 2x2 & - \\
  & 2 & Conv2D & 32 & 3x3 & relu \\
  & 2 & Conv2D & 32 & 3x3 & relu \\
 &  2 & MaxPool2D & - & 2x2 & - \\
 &  3 & Conv2D & 16 & 3x3 & relu \\ 
 &  3 & Conv2D & 4 & 2x2 & relu \\
 &  3 & MaxPool2D & - & 2x2 & - \\ \hline
\multirow{Decoder}  & 1 & Conv2D & 4 & 2x2 & relu \\ 
  & 1 & Conv2D & 16 & 3x3 & relu \\
 &  1 & UpSampling2D & - & 2x2 & - \\
 &  2 & Conv2D & 32 & 3x3 & relu \\
 &  2 & Conv2D & 32 & 3x3 & relu \\ 
 &  1 & UpSampling2D & - & 2x2 & - \\
 &  3 & Conv2D & 32 & 3x3 & relu \\ 
 &  3 & Conv2D & 64 & 3x3 & relu \\
 &  1 & UpSampling2D & - & 2x2 & - \\ \hline
\end{tabular}
\caption{Structure of Autoencoder}
\label{table:2}
\end{table}


\subsection{Process}
Throughout the research process, following Nunamaker’s framework \cite{nun}, we try several different structures for both our CNN and autoencoder models - including experimenting with the number of layers, their number of neurons, the models’ activation functions, increased dropout rates in the dropout layers, and more. The final structure provided the best results from our experimentation, while comparing how well the CNN was able to classify images, with clean, noisy, processed or unprocessed examples, as described further here in this section.
\\
\\
Defining the terms here, a ‘noisy’ example is a term we use for images where we have applied so-called ‘salt and pepper’ noise to the images. When we refer to examples being ‘clean’, we mean images where we have not added any noise to the images. The terms processed and unprocessed, refer to whether the examples have been processed through the autoencoder or not.
\\
\\
Following our research question  we start by feeding clean images from the test set to the CNN and evaluate the results, and in the next step we first process the images through the autoencoder, then the CNN and we compare the prediction rate of the two results, see \ref{fig:iteration1}.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{process-1.png}
    \caption{Feeding clear images to CNN and Autoencoder}
    \label{fig:iteration1}
\end{figure}

\noindent Furthermore we also want to evaluate how the autoencoder affects the prediction rate with noisy images. In order to do that, additionally to the previous process, we apply salt and pepper noise to the images. In this case, before evaluating the data with our CNN, we use the autoencoder’s denoising approach \cite{ae2}, by training it with clean and noisy images so the autoencoder is able to reconstruct the images (see Figure \ref{fig:iteration2}).

\begin{figure}[h]
    \centering
    \includegraphics[width=0.55\textwidth]{newprocess-2.png}
    \caption{Feeding noisy images to CNN and Autoencoder}
    \label{fig:iteration2}
\end{figure}

\noindent The visualisation of the images (Figure \ref{fig:autoencoderimages}) before- and after state helps understanding the process.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.50\textwidth]{autoencoderimages.png}
    \caption{Feeding noisy images to CNN and Autoencoder}
    \label{fig:autoencoderimages}
\end{figure}

\section{Findings}
Our main objective in this section is to compare unprocessed samples against processed samples, both for clean and noisy examples. Both the clean and noisy samples that we label as ‘processed’ have been encoded and decoded by the same autoencoder as described in the analysis section.
\\
\\
\noindent \textbf{Loss during the evaluation of the Autoencoder: 4.15\%.}
\\
\\
\noindent When evaluating the prediction accuracy the CNN achieved, we used the best-end-of-epoch model that was saved during training as explained in the analysis section. From the 10,000 examples in the test set, we feed four different datasets of images to the CNN, in two iterations,  with two datasets in each iteration. In the first iteration (Figure \ref{fig:iteration1}) the clean and noisy examples without using the autoencoder, and in the second round (Figure \ref{fig:iteration2}) running the clean and noisy examples through the autoencoder before the CNN, which resulted in deterioration of the prediction rate compared to the first iteration (see Table \ref{table:table3}).

\renewcommand{\cellalign}{l}
\begin{table}[h!]
\centering
\begin{tabular}{|l|l|l|}
\hline
 \thead {} & \thead{Clean \\examples} & \thead{Noisy \\examples} \\ 
 \hline\hline
 
\makecell{1st iteration (without \\ using autoencoder)} & 92.90 \% & 57.20 \%  \\ 
\hline
\makecell{2nd iteration (using \\ autoencoder)} & 62.92 \% & 56.21 \%  \\ 

\hline
\end{tabular}
\caption{Results of the CNN’s prediction rates}
\label{table:table3}
\end{table}

\noindent Comparing the percentage values seen in Table 3 we can conclude that clean unprocessed examples had a 29.98\% higher prediction rate than clean processed examples. While noisy unprocessed samples had a 0.99\% higher prediction rate than noisy processed examples.


\section{Conclusion}
From this experiment, we see deterioration in both clean and noisy images when processing the examples with the convolutional autoencoder. Based on that we accept our first hypothesis, as using the autoencoder negatively affects the CNN’s prediction rate, and at the same time we reject the related null-hypothesis. Regarding the second hypothesis about the effect of the autoencoder on classifying noisy images, we reject the hypothesis, as the CNN’s prediction rate did not improve after using autoencoder on noisy images. Consequently we accept the second null-hypothesis.
\\
\\
\noindent However, as we can see from the findings, evaluating the autoencoder reports some loss, so if we had an autoencoder with less loss, then we could expect the CNN to perform better using processed CNN to perform better using processed noisy images.


\newpage

\printbibliography


\end{document}

